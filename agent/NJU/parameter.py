from .env.env_def_out import UnitType

# from env.env_def_out import UnitType # 需要修改

MAP_X_LEN = 580000  # 设置小一些免得越界
MAP_Y_LEN = 580000

# MAP_X_LEN = 100000
# MAP_Y_LEN = 100000  # 需要修改

GRID_LEN = 20000

SHIP_ATTACK_DISTANCE = 116000

missile_map = {
    UnitType.A2A: 1,
    UnitType.A2G: 1,
    UnitType.UNAIRTAR: 1,
    UnitType.SHIP: 4,
    UnitType.UNSGTAR: 4,
    UnitType.S2A: 3,
}
