from ..env.env_def_out import UnitType

SHIP_AREA_PATROL_PARAMS = [270, 1, 1, 20, 10800]
AREA_PATROL_PARAMS = [270, 10000, 10000, 250, 10800]
A2A_AREA_PATROL_PARAMS = [270, 10000, 10000, 300, 10800]
AWACS_AREA_PATROL_PARAMS = [270, 1, 1, 250, 10800, 0]  # 预警机最大速度为500

RED_AIRPORT_POINT = [251004, 25526, 8000]
BLUE_SOUTH_COMMANDER_POINT = [-125000, 200000, 0]
BLUE_NORTH_COMMANDER_POINT = [-200000, -150000, 0]

# SHIP1_DEPLOY_POINT = [125000, -35864, 8000]
SHIP1_DEPLOY_POINT = [-170000, 240000, 0]  # north
SHIP2_DEPLOY_POINT = [-220000, -170000, 0]  # south

# 舰船+歼击机组合
dis_ship_x = 60000
dis_ship_y = 60000

_p_north = SHIP1_DEPLOY_POINT
_p_south = SHIP2_DEPLOY_POINT

A2A_NORTH_POINT = [_p_north[0] + dis_ship_x, _p_north[1] + dis_ship_y, 8000]
A2A_SOUTH_POINT = [_p_south[0] + dis_ship_x, _p_south[1] - dis_ship_y, 8000]

# 多出五架歼击机 北2南3
A2A_NORTH_POINT_A2A = [_p_north[0] - dis_ship_x, _p_north[1] + dis_ship_y, 8000]
A2A_SOUTH_POINT_A2A = [_p_south[0] - dis_ship_x, _p_south[1] - dis_ship_y, 8000]

# 预警机+歼击机组合
face_en_dis_1 = 50000  # 迎敌方向编队间距
vert_en_dis_1 = 100000  # 垂直地方方向编队间距

AWACS_PATROL_POINT = [-150000, 0, 8000]  # 向前压一点，不然识别距离不够
_p_awacs = AWACS_PATROL_POINT
A2A_FN_PATROL_POINT = [_p_awacs[0] + face_en_dis_1, _p_awacs[1] + vert_en_dis_1, 8000]  # front north
A2A_FC_PATROL_POINT = [_p_awacs[0] + face_en_dis_1, _p_awacs[1], 8000]  # front center
A2A_FS_PATROL_POINT = [_p_awacs[0] + face_en_dis_1, _p_awacs[1] - vert_en_dis_1, 8000]  # front south

A2G_TEAM_SIZE = 2  # 单个轰炸机编队含轰炸机个数
A2A_TEAM_SIZE = 3  # 单个歼击机编队含歼击机个数

PLANE_TYPES = [UnitType.A2A, UnitType.A2G, UnitType.AWACS, UnitType.DISTURB]

formation_name_reside_point_map = {
    'a2a_north': A2A_NORTH_POINT,
    'a2a_south': A2A_SOUTH_POINT,
    'a2a_north_more': A2A_NORTH_POINT_A2A,
    'a2a_south_more': A2A_SOUTH_POINT_A2A,
    'awacs': AWACS_PATROL_POINT,
    'a2a_fn': A2A_FN_PATROL_POINT,
    'a2a_fc': A2A_FC_PATROL_POINT,
    'a2a_fs': A2A_FS_PATROL_POINT,

}
