from agent.NJU.agent import Agent

from agent.NJU.env.env_def_out import BLUE_AIRPORT_ID
from agent.NJU.blue_player.parameters_blue import *

from agent.NJU.env.env_cmd import EnvCmd
from agent.NJU.unit_ob import UnitOb
from agent.NJU.attack_list import AttackList
from agent.NJU.formation import Formation, FormationStatus
from agent.NJU.a2g_hunt_fm import A2gHuntFormation
from agent.NJU.a2a_escort_fm import A2aEscortFormation
from copy import deepcopy


class AgentState:
    DEPLOY_STATE = 10  # 部署单位，起飞单位
    FORMATION_STATE = 20  # 编队阶段
    COMBAT_STATE = 30  # 对战阶段
    SAVE_STATE = 40  # 主守北岛阶段


TYPE2STRING_MAP = {11: "AIR", 12: "AWCS", 13: "JAM", 15: "BOM"}


class BluePlayer(Agent):
    AWACS_ESCORT_TEAM_NUM = 2

    name_cmds_map = {}  # key为编队名，value为编队包含的单位所执行过的命令

    def __init__(self, name, config):
        super().__init__(name, config['side'])
        self.side = config['side']
        self.en_side = 'red' if config['side'] == 'blue' else 'blue'
        self.attack_list = AttackList(self.side)
        self.unit_ob = UnitOb(config['side'])
        self.formation_map = {}  # key为编队名，value为编队
        self.agent_state = AgentState.DEPLOY_STATE
        self.ship_ids = [0, 0]
        self.awacs_id = 0

    def _init(self):
        self.side = 'blue'
        self.en_side = 'red'
        self.attack_list = AttackList(self.side)
        self.unit_ob = UnitOb('blue')
        self.formation_map = {}  # key为编队名，value为编队
        self.agent_state = AgentState.DEPLOY_STATE
        self.ship_ids = [0, 0]
        self.awacs_id = 0

    # 将单位分进编队里
    def add_unit2formation(self, raw_obs):
        for name, cmds in self.name_cmds_map.items():
            if name not in self.formation_map:
                if name.startswith('a2g'):
                    self.formation_map[name] = A2gHuntFormation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                                self.unit_ob)
                elif name.startswith('a2a'):
                    self.formation_map[name] = A2aEscortFormation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                                  self.unit_ob)
                else:
                    self.formation_map[name] = Formation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                         self.unit_ob)
            str_cmds = [str(cmd).replace("\'", "\"") for cmd in cmds]  # team['Task']其实是个字符串，要比较的话，需要对cmd做str
            for team in raw_obs['teams']:
                if team['Task'] in str_cmds:
                    for unit_tuple in team['PT']:
                        self.formation_map[name].add_unit(team['LX'], unit_tuple[0])

    def step_update(self, raw_obs):
        cmds = []
        self.unit_ob.update(raw_obs)  # 更新态势
        cmds_, return_ids = self.attack_list.make_update(self.unit_ob)  # 新增一个返回值，返回返航集合
        cmds.extend(cmds_)
        for _, formation in self.formation_map.items():
            formation.update(self.attack_list, self.unit_ob, return_ids)
        '''
        if self.disturb_id not in self.unit_ob.ids[self.side][UnitType.DISTURB]:
            self.disturb_id = 0
        if self.awacs_id not in self.unit_ob.ids[self.side][UnitType.AWACS]:
            self.awacs_id = 0
        if self.south_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.south_command_id = 0
        if self.north_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.north_command_id = 0
        '''
        return cmds

    def formation_patrol_attack(self, grand_target_types=None, air_target_types=None):
        if grand_target_types is None:
            grand_target_types = Formation.GRAND_ATTACK_TYPES
        if air_target_types is None:
            air_target_types = Formation.BLUE_A2A_ATTACK_TYPES
        cmds = []
        for name, formation in self.formation_map.items():
            cmd = formation.make_patrol_attack(self.attack_list, grand_target_types, air_target_types)
            # print(name, "attack_cmd", cmd)
            cmds.extend(cmd)
        return cmds

    def make_step_update(self, raw_obs):
        cmds = []
        self.unit_ob.update(raw_obs)
        cmds_, return_ids = self.attack_list.make_update(self.unit_ob)  # 新增一个返回值，返回返航集合
        cmds.extend(cmds_)

        rm_formations = []
        for name, formation in self.formation_map.items():
            formation.update(self.attack_list, self.unit_ob, return_ids)
            if formation.is_empty():
                rm_formations.append(name)

        # # 从编队字典中删除空编队。暂时这么设置，万一以后要留着往其中补充单位呢
        # for name in rm_formations:
        #     del self.formation_map[name]
        if self.awacs_id not in self.unit_ob.ids[self.side][UnitType.AWACS]:
            self.awacs_id = 0

        return cmds

        # 将单位分进编队里

    def add_unit2formation(self, raw_obs):
        for name, cmds in self.name_cmds_map.items():
            if name not in self.formation_map:
                if name.startswith('a2g'):
                    self.formation_map[name] = A2gHuntFormation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                                self.unit_ob)
                elif name.startswith('a2a'):
                    self.formation_map[name] = A2aEscortFormation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                                  self.unit_ob)
                else:
                    self.formation_map[name] = Formation(self.side, name, FormationStatus.MOVING_2_POINT, self.unit_ob)
            str_cmds = [str(cmd).replace("\'", "\"") for cmd in cmds]  # team['Task']其实是个字符串，要比较的话，需要对cmd做str
            for team in raw_obs['teams']:
                if team['Task'] in str_cmds:
                    for unit_tuple in team['PT']:
                        self.formation_map[name].add_unit(team['LX'], unit_tuple[0])

        # 所有在编队列表中的编队执行一步的操作

    def make_formation_step(self, formations=None, grand_target_types=None, air_target_types=None):
        if grand_target_types is None:
            grand_target_types = Formation.GRAND_ATTACK_TYPES
        if air_target_types is None:
            air_target_types = Formation.RED_A2A_ATTACK_TYPES
            # air_target_types = [UnitType.AWACS, UnitType.A2A]
        if formations is None:
            formations = [formation for _, formation in self.formation_map.items()]

        cmds = []
        for formation in formations:
            name = formation.name
            if name.startswith('a2a'):
                cmds.extend(formation.step(self.attack_list, air_target_types))
            elif name.startswith('a2g'):
                cmds.extend(formation.step(self.attack_list, grand_target_types))
            else:
                cmds.extend(formation.step())
        return cmds

    def step(self, sim_time, raw_obs):
        cmds = []
        cmds.extend(self.make_step_update(raw_obs))

        if self.agent_state == AgentState.DEPLOY_STATE:
            # 设置队名和命令的映射表
            self.name_cmds_map['awacs'] = []
            self.name_cmds_map['a2a_fn'] = []
            self.name_cmds_map['a2a_fc'] = []
            self.name_cmds_map['a2a_fs'] = []
            self.name_cmds_map['a2a_north'] = []
            self.name_cmds_map['a2a_south'] = []
            self.name_cmds_map['a2a_north_more'] = []
            self.name_cmds_map['a2a_south_more'] = []

            map_ = formation_name_reside_point_map
            # 起飞巡航歼击机
            for formation_name in ['a2a_fn', 'a2a_fs', 'a2a_fc', 'a2a_north', 'a2a_south', 'a2a_north_more',
                                   'a2a_south_more']:
                point = map_[formation_name]
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *point, *A2A_AREA_PATROL_PARAMS)
                self.name_cmds_map[formation_name].append(cmd)
                cmds.append(cmd)

            # 设置预警机
            for unit in raw_obs['units']:
                if unit['LX'] == UnitType.AWACS:
                    self.awacs_id = unit['ID']
                    break
            cmd = EnvCmd.make_awcs_areapatrol(self.awacs_id, *AWACS_PATROL_POINT, *AWACS_AREA_PATROL_PARAMS)
            self.name_cmds_map['awacs'].append(cmd)
            cmds.append(cmd)

            # 设置舰船
            self.ship_ids = deepcopy(self.unit_ob.ids[self.side][UnitType.SHIP])
            cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[0], *SHIP1_DEPLOY_POINT, 270, 1))
            cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[1], *SHIP2_DEPLOY_POINT, 270, 1))
            # cmds.append(EnvCmd.make_ship_areapatrol(self.ship_ids[0], *AWACS_PATROL_POINT, *SHIP_AREA_PATROL_PARAMS))

            self.agent_state = AgentState.FORMATION_STATE
            return cmds  # 这里要返回，以便于调用下一次step更新raw_obs

        if self.agent_state == AgentState.FORMATION_STATE:
            # print("unit_ob", self.unit_ob.ids)
            if sim_time < 20:  # 太早态势里找不到信息
                return cmds
            self.add_unit2formation(raw_obs)  # 将飞机编入编队中
            self.name_cmds_map.clear()
            for name, point in formation_name_reside_point_map.items():
                self.formation_map[name].set_reside_point(*point)
            #for _, formation in self.formation_map.items():
            # formation.print_formation()
            cmds.extend(self.make_formation_step())
            return cmds

        return cmds

    def reset(self):
        self._init()
