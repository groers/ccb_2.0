import numpy as np
import math
import heapq
from .env.env_def_out import UnitType
from .utils import compute_2d_distance_point
from .parameter import MAP_X_LEN, MAP_Y_LEN, GRID_LEN, SHIP_ATTACK_DISTANCE, missile_map


# from parameter import *  # 需要修改
# from utils import compute_2d_distance_point
# from unit_ob import UnitOb
# from units import Unit

OFFSETS = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]]


class InfluenceMap:
    def __init__(self, side):
        self.side = side
        self.en_side = 'blue' if side == 'red' else 'red'
        self.x_size = int(MAP_X_LEN / GRID_LEN)
        self.y_size = int(MAP_Y_LEN / GRID_LEN)
        self.id_size = self.x_size * self.y_size

        self.grid_len = GRID_LEN
        # 势力影响函数，0.707 代表二分之根号2,20000代表飞机盘旋半径, m_代表武器通道数目，dis_代表距武器平台距离
        self.inf_func = lambda m_, dis_: max(m_ * (SHIP_ATTACK_DISTANCE + self.grid_len * 0.707 + 20000 - dis_), 0)
        # self.inf_func = lambda m_, dis_: max(m_ * (30000 - dis_), 0) # 需要修改

        self.edges = [[] for _ in range(self.id_size)]  # 邻接表
        for i in range(self.x_size):
            for j in range(self.y_size):
                id_head = self.trans_2d_2_1d_id(i, j)
                x1, y1 = self.get_point(id_head)
                for offset in OFFSETS:
                    x_id = i + offset[0]
                    y_id = j + offset[1]
                    id_tail = self.trans_2d_2_1d_id(x_id, y_id)
                    if x_id < 0 or x_id >= self.x_size or y_id < 0 or y_id >= self.y_size:
                        continue
                    x2, y2 = self.get_point(x_id, y_id)
                    dis = compute_2d_distance_point(x1, y1, x2, y2)
                    self.edges[id_head].append([id_tail, dis])

        self.grand_inf_map = np.zeros([self.id_size])  # 地防和舰船形成的势力图
        self.a2a_inf_map = np.zeros([self.id_size])  # 歼击机形成的势力图
        self.a2g_inf_map = np.zeros([self.id_size])  # 轰炸机形成的势力图
        self.inf_map = np.zeros([self.id_size])  # 某些势力图相加形成的势力图形成的势力图
        self.test_gra = np.zeros([self.x_size, self.y_size])  # 需要修改

    # 将(x,y)坐标转化为二维id坐标
    def get_2d_grid_id(self, x, y):
        x_id = math.floor((x + self.grid_len / 2) / self.grid_len) + self.x_size // 2
        y_id = math.floor((y + self.grid_len / 2) / self.grid_len) + self.y_size // 2
        return [x_id, y_id]

    def trans_2d_2_1d_id(self, x_id, y_id):
        return x_id + self.x_size * y_id

    # 将(x,y)坐标转化为一维id坐标
    def get_1d_grid_id(self, x, y):
        return self.trans_2d_2_1d_id(*self.get_2d_grid_id(x, y))

    def trans_1d_2_2d_id(self, id_):
        return [id_ % self.x_size, id_ // self.x_size]

    # 根据二维或者一维单元格id获得其左边
    def get_point(self, *args):
        if len(args) == 1:
            id_ = args[0]
            id_2d = self.trans_1d_2_2d_id(id_)
            return [(id_2d[0] - self.x_size // 2) * self.grid_len, (id_2d[1] - self.y_size // 2) * self.grid_len]
        else:
            x_id = args[0]
            y_id = args[1]
            return [(x_id - self.x_size // 2) * self.grid_len, (y_id - self.y_size // 2) * self.grid_len]

    # 根据态势获得势力图
    def get_inf_map(self, unit_ob):
        type_inf_map = {
            UnitType.SHIP: self.grand_inf_map,
            UnitType.S2A: self.grand_inf_map,
            UnitType.A2G: self.a2g_inf_map,
            UnitType.A2A: self.a2a_inf_map,
        }
        en_id_map = unit_ob.id_map[self.en_side]
        for en_type, en_ids in unit_ob.ids[self.en_side].items():
            for en_id in en_ids:
                en_unit = en_id_map[en_id]
                x1, y1 = en_unit.x, en_unit.y
                for grid_id in range(self.id_size):
                    x2, y2 = self.get_point(grid_id)
                    dis = compute_2d_distance_point(x1, y1, x2, y2)
                    missile = missile_map[en_type]
                    type_inf_map[en_type][grid_id] += self.inf_func(missile, dis)

                    # TEST
                    x_id, y_id = self.trans_1d_2_2d_id(grid_id)
                    self.test_gra[self.y_size - 1 - y_id][x_id] += self.inf_func(missile, dis)
                    self.test_gra[self.y_size - 1 - y_id][x_id] = int(self.test_gra[self.y_size - 1 - y_id][x_id])
                    # TEST

    class Node:
        def __init__(self, id_, dis_multi, dis):
            self.dis = dis  # 物理距离
            self.id = id_  # 所在单元格id
            self.dis_multi = dis_multi  # 势力乘以物理距离的乘积距离
            self.dis_multi_list = [self.dis_multi, self.dis]

        def __lt__(self, other):
            return self.dis_multi_list < other.dis_multi_list

    # 寻找前往目标点的最短距离, 采用堆优化的Dijkstra算法
    def get_shortest_dis(self, target_point):
        shortest_dis = [[0xfffffffffffffffffffff, 0x7fffffff] for _ in range(self.id_size)]  # 第一个变量为乘积距离
        pre_id = [0 for _ in range(self.id_size)]  # 每个点的前驱点的id                         # 第二个变量为物理距离

        self.inf_map = self.a2a_inf_map + self.grand_inf_map
        target_id = self.get_1d_grid_id(target_point[0], target_point[1])
        heap = []
        heapq.heappush(heap, self.Node(target_id, 0, 0))
        shortest_dis[target_id] = [0, 0]
        pre_id[target_id] = target_id

        while len(heap) is not 0:
            node = heapq.heappop(heap)
            if shortest_dis[node.id] < node.dis_multi_list:  # shortest_dis[node.id]可能经过松弛后变小了，原压入堆中的路径失去价值
                continue

            # 利用最短边进行松弛
            for tuple_ in self.edges[node.id]:
                tail_id = tuple_[0]
                dis_ = tuple_[1]
                dis_multi = self.inf_map[tail_id] * dis_ + shortest_dis[node.id][0]
                present_dis = [dis_multi, dis_ + shortest_dis[node.id][1]]

                if present_dis < shortest_dis[tail_id]:
                    pre_id[tail_id] = node.id
                    shortest_dis[tail_id] = present_dis
                    heapq.heappush(heap, self.Node(tail_id, present_dis[0], present_dis[1]))

        return shortest_dis, pre_id

# if __name__ == '__main__':
#     ifu_map = InfluenceMap('red')
#     ob = UnitOb('red')
#     info_map = {
#         'ID': 1,
#         'LX': UnitType.SHIP,
#         'X': -20000,
#         'Y': 20000,
#         'Z': 8000,
#     }
#     ship = Unit(info_map)
#
#     info_map2 = {
#         'ID': 2,
#         'LX': UnitType.SHIP,
#         'X': -40000,
#         'Y': 0,
#         'Z': 8000,
#     }
#     ship2 = Unit(info_map2)
#
#     ob.id_map['blue'][1] = ship
#     ob.id_map['blue'][2] = ship2
#     ob.ids['blue'][UnitType.SHIP].append(1)
#     ob.ids['blue'][UnitType.SHIP].append(2)
#     ifu_map.get_inf_map(ob)
#     print(ifu_map.test_gra, '\n')
#     print(ifu_map.grand_inf_map)
#     a, b = ifu_map.get_shortest_dis([-40000, 0, 8000])
#
#     print(a)
#     print(b)
#
#     aid = 19
#     road = [aid]
#     while aid != 10:
#         road.append(b[aid])
#         aid = b[aid]
#     print(road)
