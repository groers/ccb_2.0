import math


# 计算两点间平面距离
def compute_2d_distance_point(x1, y1, x2, y2):
    d_x = x1 - x2
    d_y = y1 - y2
    return math.sqrt(d_x ** 2 + d_y ** 2)


# 计算点2相对于点1的方位角
def compute_2d_attack_angle_point(x1, y1, x2, y2):
    angle = 0.0
    dx = x2 - x1
    dy = y2 - y1
    if x2 == x1:
        angle = math.pi / 2.0
        if y2 == y1:
            angle = 0.0
        elif y2 < y1:
            angle = 3.0 * math.pi / 2.0
    elif x2 > x1 and y2 > y1:
        angle = math.atan(dx / dy)
    elif x2 > x1 and y2 < y1:
        angle = math.pi / 2 + math.atan(-dy / dx)
    elif x2 < x1 and y2 < y1:
        angle = math.pi + math.atan(dx / dy)
    elif x2 < x1 and y2 > y1:
        angle = 3.0 * math.pi / 2.0 + math.atan(dy / -dx)
    return int(angle * 180 / math.pi)


# （x2,y2）为被打击目标，假设敌方不动情况下，计算我方目标（x1, y1）打击地方目标时开始发弹的点,hunt_id为我方开火距离
def compute_2d_battle_point(x1, y1, x2, y2, hunt_dis):
    angle = compute_2d_attack_angle_point(x2, y2, x1, y1)  # 方位角
    x3 = x2 + math.sin(angle / 180 * math.pi) * hunt_dis
    y3 = y2 + math.cos(angle / 180 * math.pi) * hunt_dis
    return round(x3), round(y3)


# 判断是否运行过程中会被船打击到
def will_attack_by_ship(x1, y1, x2, y2, x_c, y_c, r_c=130000):
    flag1 = (x1 - x_c) * (x1 - x_c) + (y1 - y_c) * (y1 - y_c) <= r_c * r_c
    flag2 = (x2 - x_c) * (x2 - x_c) + (y2 - y_c) * (y2 - y_c) <= r_c * r_c
    if flag1 and flag2:  # 情况一、两点都在圆内 :一定会被达到
        return True
    elif flag1 or flag2:  # 情况二、一个点在圆内，一个点在圆外：一定相交
        return True
    else:  # 情况三、两个点都在圆外
        # 将直线p1p2化为一般式：Ax+By+C=0的形式。先化为两点式，然后由两点式得出一般式
        A = y1 - y2
        B = x2 - x1
        C = x1 * y2 - x2 * y1
        # 使用距离公式判断圆心到直线ax+by+c=0的距离是否大于半径
        dist1 = A * x_c + B * y_c + C
        dist1 *= dist1
        dist2 = (A * A + B * B) * r_c * r_c
        if dist1 > dist2:  # 圆心到直线p1p2的距离大于半径，不相交
            return False
        angle1 = (x_c - x1) * (x2 - x1) + (y_c - y1) * (y2 - y1)
        angle2 = (x_c - x2) * (x1 - x2) + (y_c - y2) * (y1 - y2)
        if angle1 > 0 and angle2 > 0:  # 余弦为正，则是锐角，一定相交
            return True
        else:
            return False
